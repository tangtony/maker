package ldh.maker.component;

import javafx.scene.control.Tab;
import javafx.scene.control.TreeItem;
import ldh.database.Table;
import ldh.maker.code.CreateCode;
import ldh.maker.code.FlutterCreateCode;
import ldh.maker.vo.SettingData;
import ldh.maker.vo.TreeNode;

/**
 * Created by ldh123 on 2018/9/10.
 */
public class FlutterContentUi extends TableUi {

    public FlutterContentUi() {
    }

    @Override
    protected Tab createContentTab(String selectTable) {
        Tab tab = new Tab();
        tab.setText(selectTable);
        String db = treeItem.getValue().getData().toString();
        CodeUi codeUi = new FlutterCodeUi(treeItem, db, selectTable);
        tab.setContent(codeUi);
        return tab;
    }

    @Override
    protected CreateCode buildCreateCode(SettingData data, TreeItem<TreeNode> treeItem, String dbName, Table table) {
        return new FlutterCreateCode(data, treeItem, dbName, table);
    }

    @Override
    protected SettingPane buildSettingPane(TreeItem<TreeNode> treeItem, String dbName) {
        return new FlutterSettingPane(treeItem, dbName);
    }

    @Override
    protected boolean isSetting(String dbName) {
        return true;
    }
}
