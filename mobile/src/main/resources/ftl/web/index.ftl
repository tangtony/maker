import Vue from 'vue'
import Router from 'vue-router'
import Main from '@/components/Main'

<#list tableInfo.tables?keys as key>
  <#if !tableInfo.tables[key].middle && tableInfo.tables[key].create && (tableInfo.tables[key].primaryKey?? && !tableInfo.tables[key].primaryKey.composite)>
import ${util.firstUpper(tableInfo.tables[key].javaName)}ListPage from '@/pages/${util.firstLower(tableInfo.tables[key].javaName)}/${util.firstUpper(tableInfo.tables[key].javaName)}List'
import ${util.firstUpper(tableInfo.tables[key].javaName)}ViewPage from '@/pages/${util.firstLower(tableInfo.tables[key].javaName)}/${util.firstUpper(tableInfo.tables[key].javaName)}View'
  </#if>
</#list>

Vue.use(Router)

export default new Router({
  routes: [
    { path: '/', name: 'Main', component: Main,
      children:[
        <#list tableInfo.tables?keys as key>
          <#if !tableInfo.tables[key].middle && tableInfo.tables[key].create && (tableInfo.tables[key].primaryKey?? && !tableInfo.tables[key].primaryKey.composite)>
        { path: '${util.firstLower(tableInfo.tables[key].javaName)}/list', component:${util.firstUpper(tableInfo.tables[key].javaName)}ListPage },
        { path: '${util.firstLower(tableInfo.tables[key].javaName)}/view', component:${util.firstUpper(tableInfo.tables[key].javaName)}ViewPage }<#if key_has_next>,</#if>
          </#if>
        </#list>
      ]
    }
  ]
})
