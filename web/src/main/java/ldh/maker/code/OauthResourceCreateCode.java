package ldh.maker.code;

import javafx.scene.control.TreeItem;
import ldh.database.Table;
import ldh.maker.database.TableInfo;
import ldh.maker.freemaker.*;
import ldh.maker.util.DbInfoFactory;
import ldh.maker.util.FileUtil;
import ldh.maker.util.FreeMakerUtil;
import ldh.maker.vo.DBConnectionData;
import ldh.maker.vo.SettingData;
import ldh.maker.vo.SettingJson;
import ldh.maker.vo.TreeNode;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class OauthResourceCreateCode extends WebCreateCode {

    public OauthResourceCreateCode(SettingData data, TreeItem<TreeNode> treeItem, String dbName, Table table) {
        super(data, treeItem, dbName, table);
        isOauth = true;
        addData("controllerFtl", "/oauthResource/controller.ftl");

        if (data.getSettingJson().isShiro()) {
//            add("/freemarkerOauth/login.ftl", "login.ftl", "ftl");
        }
    }

    @Override
    protected void createOther(){
        super.createOther();

        if (table.isMiddle()) return;

    }

    @Override
    protected void createOnce(){
        super.createOnce();

        String projectRootPackage = getProjectRootPackage(data.getPojoPackageProperty());
        String configPath = createPath(projectRootPackage + ".configuration");
        new SimpleJavaMaker()
                .projectRootPackage(projectRootPackage)
                .auhtor(data.getAuthorProperty())
                .ftl("/oauthResource/WebResourceServerConfiguration.ftl")
                .fileName("WebResourceServerConfiguration.java")
                .outPath(configPath)
                .make();

        new SimpleJavaMaker()
                .projectRootPackage(projectRootPackage)
                .auhtor(data.getAuthorProperty())
                .ftl("/freemarkerOauth/ehcache.ftl")
                .fileName("ehcache.xml")
                .outPath(createResourcePath())
                .make();

        new SimpleJavaMaker()
                .projectRootPackage(projectRootPackage)
                .auhtor(data.getAuthorProperty())
                .ftl("/freemarkerOauth/ehcache.ftl")
                .fileName("ehcache.xml")
                .outPath(createResourcePath("resources-prod"))
                .make();

        try {
            SettingJson settingJson = data.getSettingJson();
            if (settingJson.isShiro()) {
                makerShiro();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void makerShiro() {
        String projectRootPackage = getProjectRootPackage(data.getPojoPackageProperty());
        String configPath = createPath(projectRootPackage + ".configuration");

        new SimpleJavaMaker()
                .projectRootPackage(projectRootPackage)
                .auhtor(data.getAuthorProperty())
                .ftl("/oauthResource/SecurityConfiguration.ftl")
                .fileName("SecurityConfiguration.java")
                .outPath(configPath)
                .make();
    }


    @Override
    protected void buildPomXmlMaker(String path) {
        String projectRootPackage = getProjectRootPackage(data.getPojoPackageProperty());
        String resourcePath = createPomPath();
        new PomXmlMaker()
                .projectRootPackage(projectRootPackage)
                .projectName(this.getProjectName())
                .ftl("/oauthResource/pom.ftl")
                .outPath(resourcePath)
                .make();
    }

    protected void buildApplicationPropertiesMaker() {

    }

    protected void buildApplicationYmlMaker() {
        String projectRootPackage = getProjectRootPackage(data.getPojoPackageProperty());
        String resourcePath = createResourcePath();
        TreeNode treeNode = treeItem.getValue().getParent();
        DBConnectionData data = (DBConnectionData) treeNode.getData();
        new ApplicationPropertiesMaker()
                .projectRootPackage(projectRootPackage)
                .projectName(getProjectName())
                .dBConnectionData(data)
                .ftl("oauthResource/application.ftl")
                .dbName(dbName)
                .fileName("application.yml")
                .outPath(resourcePath)
                .make();

        resourcePath = createResourcePath("resources-dev");
        new ApplicationPropertiesMaker()
                .projectRootPackage(projectRootPackage)
                .projectName(getProjectName())
                .dBConnectionData(data)
                .ftl("oauthResource/application.ftl")
                .fileName("application.yml")
                .dbName(dbName)
                .outPath(resourcePath)
                .make();

        resourcePath = createResourcePath("resources-prod");
        new ApplicationPropertiesMaker()
                .projectRootPackage(projectRootPackage)
                .projectName(getProjectName())
                .dBConnectionData(data)
                .ftl("oauthResource/application.ftl")
                .fileName("application.yml")
                .dbName(dbName)
                .outPath(resourcePath)
                .make();

        resourcePath = createTestResourcePath("resources");
        new ApplicationPropertiesMaker()
                .projectRootPackage(projectRootPackage)
                .projectName(this.getProjectName())
                .dBConnectionData(data)
                .ftl("applicationTest.ftl")
                .fileName("application.yml")
                .dbName(dbName)
                .outPath(resourcePath)
                .make();

    }




    public String getProjectName() {
        return data.getProjectNameProperty();
    }
}
