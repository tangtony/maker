package ldh.maker.code;

import javafx.scene.control.TreeItem;
import ldh.database.Table;
import ldh.maker.database.TableInfo;
import ldh.maker.freemaker.*;
import ldh.maker.util.DbInfoFactory;
import ldh.maker.util.FreeMakerUtil;
import ldh.maker.vo.SettingData;
import ldh.maker.vo.TreeNode;

import java.util.*;

import static java.lang.System.arraycopy;

/**
 * Created by ldh on 2017/4/6.
 */
public class WebCreateCode extends CreateCode {

    protected List<String> jsFtls = new ArrayList<String>();
    protected Map<String, String> jspFtls = new HashMap<>();
    protected Map<String, String> otherData = new HashMap<>();

    protected boolean isOauth = false;

    public WebCreateCode(SettingData data, TreeItem<TreeNode> treeItem, String dbName, Table table) {
        super(data, treeItem, dbName, table);
    }

    public void addData(String key, String ftl) {
        otherData.put(key, ftl);
    }

    @Override
    protected void createOther(){
        if (table.isMiddle()) return;
        String controllerPath = createPath(data.getControllerPackageProperty());
        String beanName = FreeMakerUtil.firstUpper(table.getJavaName());
        WebControllerMaker controllerMaker = new WebControllerMaker()
                .pack(data.getControllerPackageProperty())
                .author(data.getAuthorProperty())
                .description(table.getComment())
                .outPath(controllerPath)
                .dbName(dbName)
                .treeId(treeItem.getParent().getValue().getId())
                .className(beanName + "Controller")
                .beanMaker(pojoMaker)
                .beanWhereMaker(pojoWhereMaker)
//                .service(serviceInterfaceMaker)
                .imports(pojoWhereMaker)
                .ftl(otherData.get("controllerFtl"))
//                .shiro(data.getSettingJson().isShiro())
                .table(table);
        if (isOauth) {
            controllerMaker.oauth(data.getSettingJson().isShiro());
        } else {
            controllerMaker.shiro(data.getSettingJson().isShiro());
        }
        if (data.getServiceInterface()) {
            controllerMaker.service(serviceInterfaceMaker);
        } else {
            controllerMaker.service(serviceMaker);
        }

        controllerMaker.make();

        if (jspFtls != null) {
            for (Map.Entry<String, String> ftl : jspFtls.entrySet()) {
                String db = treeItem.getValue().getData().toString();
                TableInfo tableInfo = DbInfoFactory.getInstance().get(treeItem.getValue().getParent().getId() + "_" + db);
                String t = FreeMakerUtil.firstLower(table.getJavaName());
                String fileName = t.toLowerCase() + ftl.getValue() + ".jsp";
                String jspPath = createJspPath("jsp");
                new JspMaker()
                        .tableInfo(tableInfo)
                        .table(table)
                        .outPath(jspPath)
                        .shiro(data.getSettingJson().isShiro())
                        .ftl(ftl.getKey())
                        .fileName(fileName)
                        .make();
            }
        }

        if (jsFtls != null) {
            String jsPath = createJsPath();
            for (String jsFtl : jsFtls) {
                new JsMaker()
                        .table(table)
                        .outPath(jsPath)
                        .ftl(jsFtl)
                        .make();
            }
        }
    }

    @Override
    protected void createOnce() {
        super.createOnce();
        if (resourceMap != null) {
            for (Map.Entry<String, String[]> entry : resourceMap.entrySet()) {
                String[] dirs = new String[entry.getValue().length-1];
                arraycopy(entry.getValue(), 1, dirs, 0, dirs.length);
                String path = createJspPath(dirs);
                String db = treeItem.getValue().getData().toString();
                TableInfo tableInfo = DbInfoFactory.getInstance().get(treeItem.getValue().getParent().getId() + "_" + db);
                new JspMainMaker()
                        .tableInfo(tableInfo)
                        .ftl(entry.getKey())
                        .outPath(path)
                        .shiro(data.getSettingJson().isShiro())
                        .fileName(entry.getValue()[0])
                        .make();
            }
        }
    }

    protected synchronized void mainPage(String jspPath) {
        String db = treeItem.getValue().getData().toString();
        TableInfo tableInfo = DbInfoFactory.getInstance().get(treeItem.getValue().getParent().getId() + "_" + db);
        new JspMainMaker()
                .tableInfo(tableInfo)
                .outPath(jspPath)
                .make();
    }

    public String getProjectName() {
        return data.getProjectNameProperty();
    }
}
