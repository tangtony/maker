package ldh.maker.component.oauth;

import javafx.scene.control.TreeItem;
import ldh.maker.component.PojoTableUi;
import ldh.maker.vo.TreeNode;

/**
 * Created by ldh123 on 2018/5/6.
 */
public class FreemarkerOauthTableUi extends PojoTableUi {

    public FreemarkerOauthTableUi(TreeItem<TreeNode> treeItem, String dbName) {
        super(treeItem, dbName);
    }

}
