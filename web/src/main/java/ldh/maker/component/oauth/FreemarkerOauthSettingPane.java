package ldh.maker.component.oauth;

import javafx.scene.control.Tab;
import javafx.scene.control.TreeItem;
import ldh.maker.component.FreemarkerTableUi;
import ldh.maker.component.SettingPane;
import ldh.maker.vo.TreeNode;

/**
 * Created by ldh123 on 2018/5/6.
 */
public class FreemarkerOauthSettingPane extends SettingPane {

    private FreemarkerOauthTableUi freemarkerTableUi;

    public FreemarkerOauthSettingPane(TreeItem<TreeNode> treeItem, String dbName) {
        super(treeItem, dbName);

        buildTableUiTab();
    }

    private void buildTableUiTab() {
        if (freemarkerTableUi == null) {
            freemarkerTableUi = new FreemarkerOauthTableUi(treeItem, dbName);
        }
        Tab tab = createTab("Table设置", freemarkerTableUi);
        tab.selectedProperty().addListener((b,o,n)->{
            if (tab.isSelected()) {
                freemarkerTableUi.show();
            }
        });
    }
}
