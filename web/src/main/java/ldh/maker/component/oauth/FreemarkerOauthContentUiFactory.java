package ldh.maker.component.oauth;

import ldh.maker.component.ContentUi;
import ldh.maker.component.ContentUiFactory;
import ldh.maker.component.FreemarkerContentUi;

public class FreemarkerOauthContentUiFactory extends ContentUiFactory {

    public ContentUi create() {
        return new FreemarkerOauthContentUi();
    }
}
