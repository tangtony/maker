package ldh.maker;

import ldh.maker.component.oauth.OauthResourceContentUiFactory;
import ldh.maker.util.UiUtil;

public class OauthResourceWebMain extends ServerMain {

    @Override
    protected void preHandle() {
        UiUtil.setContentUiFactory(new OauthResourceContentUiFactory());
        UiUtil.setType("OauthResource");
    }

    @Override
    protected String getTitle() {
        return "智能代码生成器之生成spring oauth2 resource代码 ";
    }

    public static void main(String[] args) throws Exception {
        throw new RuntimeException("请运行MainLauncher");
//        startDb(null);
//        launch(args);
    }
}

