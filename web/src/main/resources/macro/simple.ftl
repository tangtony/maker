<#--公共头部-->
<#assign ctx=springMacroRequestContext.contextPath />
<#macro header title keywords="" description="">
<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width; initial-scale=1.0; shrink-to-fit=no; maximum-scale=1.0;" />
        <meta name="format-detection" content="telephone=no" />
        <title>${title}</title>
        <meta name="keywords" content="${keywords}" />
        <meta name="description" content="${description}" />

        <link href="${ctx}/webjars/bootstrap/4.3.1/css/bootstrap.css" rel="stylesheet">
        <#nested>

    </head>
<body>
</#macro>

<#macro body>
    <#nested>
</#macro>
<#--公共底部-->
<#macro footer>
    <script src="${ctx}/webjars/jquery/3.0.0/jquery.js"></script>
    <script src="${ctx}/webjars/bootstrap/4.3.1/js/bootstrap.js"></script>
    <#nested>
</body>
</html>
</#macro>