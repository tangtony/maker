<%@ tag language="java" pageEncoding="utf-8"%>
<%@ attribute name="headContent" fragment="true" required="false" %>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>代码自动生成测试平台</title>
    <link href="${pageContext.request.contextPath}/resource/frame/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resource/frame/bootstrap/css/dashboard.css" rel="stylesheet">

    <jsp:invoke fragment="headContent"/>
</head>

<body>
    <jsp:doBody/>

    <script src="${pageContext.request.contextPath}/resource/common/js/jquery-1.8.0.min.js"></script>
    <script src="${pageContext.request.contextPath}/resource/frame/bootstrap/js/bootstrap.js"></script>
</body>
</html>
