spring:
  profiles:
    active: dev

  datasource:
    driver-class-name: com.mysql.cj.jdbc.Driver
    url: jdbc:mysql://${dbConnectionData.ipProperty}:${dbConnectionData.portProperty?c}/${dbName}?serverTimezone=UTC&useUnicode=true&characterEncoding=utf-8&useSSL=false&allowPublicKeyRetrieval=true&zeroDateTimeBehavior=CONVERT_TO_NULL
    username: ${dbConnectionData.userNameProperty}
    password: ${dbConnectionData.passwordProperty}
    hikari:
      minimum-idle: 5
      maximum-pool-size: 15
      auto-commit: true
      idle-timeout: 30000
      pool-name: DatebookHikariCP
      max-lifetime: 1800000
      connection-timeout: 30000
      connection-test-query: SELECT 1

  freemarker:
    template-loader-path: classpath:/templates,classpath:/ftl
    allow-request-override: false
    cache: false
    check-template-location: true
    charset: utf-8
    content-type: text/html
    expose-request-attributes: false
    expose-session-attributes: false
    expose-spring-macro-helpers: true
    request-context-attribute: request

  mvc:
    static-path-pattern: /resource/**

  resources:
    static-locations: classpath:/resource/

server:
  port: 8080