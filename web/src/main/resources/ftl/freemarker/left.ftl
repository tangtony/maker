${r'<#'}assign ctx=springMacroRequestContext.contextPath />
<nav class="col-md-2 d-none d-md-block bg-light sidebar">
    <div class="sidebar-sticky">
        <ul class="nav flex-column">
            <#list tableInfo.tables?keys as key>
            <#if !tableInfo.tables[key].middle && tableInfo.tables[key].create>
            <#if shiro>${r'<@'}shiro.hasPermission name="${util.lowers(tableInfo.tables[key].javaName)}:list"></#if>
            <li class="nav-item">
                <a href="${r'${'}ctx}/${util.firstLower(tableInfo.tables[key].javaName)}/list" class="nav-link  <${r'#'}if active?? && active=="${util.lowers(tableInfo.tables[key].javaName)}">active</${r'#'}if>">
                    <i class="fa fa-address-card" aria-hidden="true" style="margin-right: 5px"></i>${util.comment(tableInfo.tables[key])}管理<span class="sr-only"></span>
                </a>
            </li>
            <#if shiro>${r'</'}@shiro.hasPermission></#if>
            </#if>
            </#list>
        </ul>
    </div>

</nav>