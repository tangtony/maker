package ldh.maker.freemaker;

import ldh.maker.database.TableInfo;

/**
 * Created by ldh on 2017/4/16.
 */
public class JavafxFxmlMaker extends BeanMaker<JavafxFxmlMaker> {

    @Override
    public JavafxFxmlMaker make() {
        data();
        out(ftl, data);

        return this;
    }

    @Override
    public void data() {
    }
}
