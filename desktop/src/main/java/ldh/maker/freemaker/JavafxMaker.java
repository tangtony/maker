package ldh.maker.freemaker;

import ldh.maker.database.TableInfo;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by ldh on 2017/4/16.
 */
public class JavafxMaker extends BeanMaker<JavafxMaker> {

    protected TableInfo tableInfo;
    protected String projectPackage;
    protected String fileName;
    protected String author;

    public JavafxMaker tableInfo(TableInfo tableInfo) {
        this.tableInfo = tableInfo;
        return this;
    }

    public JavafxMaker projectPackage(String projectPackage) {
        this.projectPackage = projectPackage;
        return this;
    }

    public JavafxMaker author(String author) {
        this.author = author;
        return this;
    }

    @Override
    public JavafxMaker make() {
        data();
        out(ftl, data);

        return this;
    }

    @Override
    public void data() {
        data.put("tableInfo", tableInfo);
        data.put("table", table);
        data.put("projectPackage", projectPackage);
        data.put("controllerPackage", pack);
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String str=sdf.format(new Date());
        data.put("Author", author);
        data.put("DATE", str);
    }
}
