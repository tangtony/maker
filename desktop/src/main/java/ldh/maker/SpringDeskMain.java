package ldh.maker;

import ldh.maker.component.SpringJavafxContentUiFactory;
import ldh.maker.util.UiUtil;

public class SpringDeskMain extends ServerMain {

    @Override
    protected void preHandle() {
        UiUtil.setContentUiFactory(new SpringJavafxContentUiFactory());
        UiUtil.setType("springdesk");
    }

    protected String getTitle() {
        return "智能代码生成器之生成spring boot + javafx client代码";
    }

    public static void main(String[] args) throws Exception {
        startDb(null);
        launch(args);
    }
}

